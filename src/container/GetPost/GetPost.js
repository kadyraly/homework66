import React, {Component}from 'react';
import './GetPost.css';
import axios from '../../axios';
import withErrorHandler from "../../hoc/withErrorHandler/withErrorHandler";


class GetPost extends Component {
    state = {
        posts: {},
        loading: false
    };

    componentDidMount() {
        this.getPost();
    };

    getPost = () => {
        this.setState({loading: true});
        axios.get('/posts.json').then((response) => {
            if(response) {
                this.setState({posts: response.data})
            }
        })
    };

    postDeleteHandler = (postId) => {
        console.log(postId);
        axios.delete(`/posts/${postId}.json`).then(() => {
            this.getPost();
        })

    };



    render () {
        let postsId = null;
        if(this.state.posts) {
            postsId = Object.keys(this.state.posts);
        }
        return(
            <div className="post">
                {this.state.posts ? postsId.map((postId) => {
                return <div key={postId}>
                    <h2>{this.state.posts[postId].title}</h2>
                    <p>{this.state.posts[postId].body}</p>
                    <button onClick= {() => this.postDeleteHandler(postId)}>X</button>
                </div>
                }) : null}
            </div>
       )

    };
}

export default withErrorHandler(GetPost, axios);