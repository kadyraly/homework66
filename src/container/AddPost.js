import React, {Component} from 'react';
import './AddPost.css';
import axios from '../axios';

class AddPost extends Component {
    state = {
        title: '',
        body: '',
        loading: false
    };


    handlePostChange = event => {
        event.persist();
        this.setState({[event.target.name]: event.target.value});
    };


    postSaveHandler = (e) => {
        e.preventDefault();
        this.setState({loading: true});

        axios.post('/posts.json', {title: this.state.title, body: this.state.body}).then(() => {

        }).finally(() => {
            this.setState({loading: false});
        });


    };
    render () {
        return (
            <form className="add">
                <input type="text" name="title"  value={this.state.title} onChange={this.handlePostChange} />
                <textarea  name="body"  value={this.state.body} onChange={this.handlePostChange} />
                <button onClick={this.postSaveHandler}>Save</button>
            </form>
        );
    }

}

export default AddPost;