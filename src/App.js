import React, { Component, Fragment } from 'react';
import AddPost from "./container/AddPost";
import GetPost from "./container/GetPost/GetPost";

class App extends Component {



    render() {
        return (
            <Fragment>
                <AddPost />
                <GetPost/>
            </Fragment>

        );
    }
}

export default App;

