import React, {Component, Fragment} from "react";
import Modal from "../../component/UI/Modal/Modal";
import Spinner from "../../component/UI/Spinner/Spinner";


const withErrorHandler = (WrappedComponent, axios) => {
    return  class WithErrorHandler extends Component  {

        constructor(props) {
            super(props);

            this.state = {
                error: null,
                loading: false
            };

            axios.interceptors.request.use(req => {
                this.setState({loading: true});
                return req;
            }, error => {
                this.setState({error: error, loading: false})
            });

            this.state.id = axios.interceptors.response.use(res => {
                this.setState({loading: false});
                return res;
            }, error => {
                this.setState({error: error, loading: false})
            });
        }

        errorDismissed = () => {
            this.setState({error: null});
        };

        componentWillUnmount() {
            axios.interceptors.response.eject(this.state.id);
        }

        render() {
            return (
                <Fragment>
                    {this.state.loading ? <Spinner /> : null}
                    <Modal show={this.state.error} closed={this.errorDismissed}>
                        {this.state.error ? this.state.error.message : null}
                    </Modal>
                    <WrappedComponent {...this.props} />
                </Fragment>
            );
        }


    };
};

export default withErrorHandler;